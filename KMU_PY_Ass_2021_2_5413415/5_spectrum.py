Type = ''
wavelength = float(input('Wavelength : '))
freq = float(input('Frequency : '))

if wavelength > 1E-1 or freq < 3*(1E+9):
    Type = 'Radio Waves'
elif (1E-3 <= wavelength <= 1E-1) or (3*(1E+9) <= freq <= 3*(1E+11)) :
    Type = 'Microwaves'
elif (7*(1E-7) <= wavelength <= 1E-3) or (3*(1E+11) <= freq <= 4*(1E+14)) :
    Type = 'Infrared'
elif (4*(1E-7) <= wavelength <= 7*(1E-7)) or (4*(1E+14) <= freq <= 7.5*(1E+14)) :
    Type = 'Visible Light'
elif (1E-8 <= wavelength <= 4*(1E-7)) or 7.5*(1E+14) <= freq <= 3*(1E+16) :
    Type = 'Ultraviolet'
elif (1E-11 <= wavelength <= 1E-8) or 3*(1E+16) <= freq <= 3*(1E+19) :
    Type = 'X-rays'
elif wavelength < 1E-11 or freq > 3*(1E+19):
    Type = 'Gamma Rays'

print('Type : ', Type)

