from ezgraphics import GraphicsWindow

win = GraphicsWindow(400, 400)
win.setTitle("face")

canvas = win.canvas()
canvas.setOutline("green")
canvas.drawOval(100, 100, 200, 200)

canvas.setOutline("green")
canvas.setFill("yellow")
canvas.drawOval(130, 150, 50, 20)
canvas.setFill("black")
canvas.drawOval(150, 155, 10, 10)

canvas.setOutline("green")
canvas.setFill("yellow")
canvas.drawOval(220, 150, 50, 20)
canvas.setFill("black")
canvas.drawOval(240, 155, 10, 10)

canvas.setOutline("green")
canvas.setFill("yellow")
canvas.drawPolygon(200,190,190,230,210,230)

canvas.setOutline("green")
canvas.setFill("yellow")
canvas.drawOval(165, 250, 65, 25)

win.wait()