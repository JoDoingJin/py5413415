import numpy as np

a=17.27
b=237.7
T=float(input('temperature(degrees C) : '))
RH=float(input('relative humidity(0~1) : '))
f=(a*T)/(b+T)+np.log(RH)
dew_point=(b*f)/(a-f)
print(dew_point)