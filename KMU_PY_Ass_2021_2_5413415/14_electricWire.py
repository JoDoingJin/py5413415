import math

def diameter(wireGauge):
    return 0.127*92**((16-wireGauge)/39)
def copperWireResistance(length, wireGauge):
    return (4*1.678*10**-8*length)/(math.pi*diameter(wireGauge)**2)

def main():
    length = float(input("wire length"))
    wireGauge = float(input("wire gauge : "))
    print("The copper wire resistance : ",copperWireResistance(length, wireGauge))

main()