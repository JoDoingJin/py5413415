def swapFisrtLast(list):
    temp1=list[0]
    temp2=list[-1]
    list[0]=temp2
    list[-1]=temp1
    return list

def shiftElement(list):
    list.insert(0,list[-1])
    list.pop(-1)
    return list

def replaceEven(list):
    for i in range(len(list)):
        if i%2==0:
            list[i]=0
    return list

def removeMiddle(list):
    if len(list)%2==0:
        list.pop(len(list)/2)
        list.pop(len(list)/2+1)
    else:
        list.pop(len(list)/2)
    return list
def moveFront(list):
    for i in range(len(list)):
        if list[i]%2==0:
            list.insert(0,list[i])
            list.pop(i+1)
    return list

def secondLargest(list):
    list.sort()
    return list[1]

def isSorted(list):
    list_sort=list.sort()
    return list==list_sort

def containsAdjacent(list):
    for i in range(len(list)):
        if list[i]==list[i+1]:
            return True
def containsDuplicate(list):
    for i in range(len(list)):
        if list[0]==list[i+1]:
            return True