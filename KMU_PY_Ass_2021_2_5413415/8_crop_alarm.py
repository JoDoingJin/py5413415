import numpy as np

R0 = 33192
T0 = 40
R2, R3, R4 = 156300, 156300, 156300
beta = 3310-273
T = float(input('temperature(F) : '))
T = (T-32)/1.8
R = R0*np.exp(beta*(1/T+1/T0))

V_in = R2/(R+R2)
V_ref = R4/(R3+R4)

print
if V_in < V_ref:
    print('alarm')
