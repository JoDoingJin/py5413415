import math

def sphereVolume(r) :
    return (4/3)*math.pi*r**3
def sphereSurface(r) :
    return 4*math.pi*r**2
def cylinderVolume(r, h) :
    return math.pi*r**2*h
def cylinderSurface(r,h) :
    return 2*math.pi*r*h + 2*math.pi*r**2
def coneVolume(r, h) :
    return (1/3)*math.pi*r**2*h
def coneSurface(r, h) :
    return math.pi*r**2+math.pi*r*(h**2+r**2)**0.5

def main():
    print("1. sphere volume \n2. sphere surface ")
    print("3. cylinder volume \n4. cyliner surface ")
    print("5. cone volume \n6. cone surface ")
    x=int(input("Enter number to get figure's volume or surface :"))
    if x==1:
        r=float(input("radius : "))
        print("sphereVolume :", sphereVolume(r))
    elif x==2:
        r=float(input("radius : "))
        print("sphereSurface :", sphereSurface(r))
    elif x==3:
        r=float(input("radius : "))
        h=float(input("height : "))
        print("cylinderVolume :", cylinderVolume(r, h))

    elif x==4:
        r=float(input("radius : "))
        h=float(input("height : "))
        print("cylinderSurface :", cylinderSurface(r, h))
    elif x==5:
        r=float(input("radius : "))
        h=float(input("height : "))
        print("coneVolume :",coneVolume(r, h))
    elif x==6:
        r=float(input("radius : "))
        h=float(input("height : "))
        print("coneSurface :",coneSurface(r, h))

main()